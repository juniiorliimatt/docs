# WSL 2

## Tutorial

[https://github.com/codeedu/wsl2-docker-quickstart](https://github.com/codeedu/wsl2-docker-quickstart)

## Debian WSL

## Etapa 1 – Habilitar o Subsistema do Windows para Linux

```powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
```

## Etapa 2 – Verificar os requisitos para executar o WSL 2

- Para sistemas x64: **Versão 1903** ou superiores, com o **Build 18362** ou superiores.
- Para sistemas ARM64: **Versão 2004** ou superiores, com o **Build 19041** ou superiores.

Atualizar todo o sistema.

## Etapa 3 – Habilitar o recurso de Máquina Virtual

```powershell
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

REINICIAR A MÁQUINA
```

## Etapa 4 – Baixar o pacote de atualização do kernel do Linux

[https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) após, instalar, reiniciar a máquina.

## Etapa 5 – Definir o WSL 2 como a sua versão padrão

```powershell
wsl --set-default-version 2
```

Etapa 6 – Instalar a distribuição do Linux de sua escolha

Ir na Loja do windows, e instalar o debian

Após a instalação do Debian, atualizar a disto

```bash
sudo apt update -y && sudo apt upgrade -y
```

instalar o build-essential

```bash
sudo apt install build-essential -y
```

instalar o git

```bash
sudo apt install git -y && sudo apt install git-flow -y
```

configuração do git

```bash
[core]
 editor = code
 autocrlf = input
[user]
 name = Junior Lima
 email = juniiorliimatt@gmail.com
[init]
 defaultBranch = main
[color]
    ui = auto
[color "branch"]
    current = yellow reverse
    local = yellow
    remote = green
[color "diff"]
    meta = yellow bold
    frag = magenta bold
    old = red bold
    new = green bold
[color "status"]
    added = yellow
    changed = green
    untracked = cyan
[credential]
 helper = manager-core
```

Instalar o dos2unix, para ajudar com o CRLF

```bash
sudo apt install dos2unix
```

instalar o Java 11

```bash
sudo apt install openjdk-11-jdk -y
```

Instalar o maven

```bash
sudo apt install maven -y
```

após instalar no node LTS

```bash
sudo apt install curl -y curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
apt-get install -y nodejs
```

## Instalando o oh-my-zsh

Instalar o zsh

```bash
sudo apt install zsh -y
```

Definir o shell padrão

```bash
chsh -s $(which zsh)
```

Reiniciar o terminal

Instalar o Oh My Zsh

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

Reiniciar o terminal

Instalar o Spaceship

```bash
git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
```

Trocar a fonte.

Criar um link simbólico

```bash
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
```

Editar o arquivo ./zshrc que fica na pasta home

```bash
code ./zshrc

ZSH_THEME="spaceship"
```

Definir o tema no arquivo .zshrc na linha da opção **ZSH_THEME por "spaceship", salvar e reiniciar o terminal, após reiniciar, voltar no arquivo .zshrc e adicionar o seguite no final do arquivo:**

SPACESHIP_PROMPT_ORDER=(
  user          # Username section
  dir           # Current directory section
  host          # Hostname section
  git           # Git section (git_branch + git_status)
  hg            # Mercurial section (hg_branch  + hg_status)
  exec_time     # Execution time
  line_sep      # Line break
  vi_mode       # Vi-mode indicator
  jobs          # Background jobs indicator
  exit_code     # Exit code section
  char          # Prompt character
)
SPACESHIP_USER_SHOW=always
SPACESHIP_PROMPT_ADD_NEWLINE=false
SPACESHIP_CHAR_SYMBOL="❯"
SPACESHIP_CHAR_SUFFIX=" "

Depois por os plugins do ZSH

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"
```

Após terminar de instalar, reiniciar novamente o terminal, e adicionar as seguintes linhas no final do arquivo .zshrc

zinit light zdharma/fast-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions

[voltar](../README.md)
