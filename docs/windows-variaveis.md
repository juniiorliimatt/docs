# Variáveis de ambiente úteis

Nome da variável: API_EXPIRATIONS_MS  
Valor da variável: 86400000  

Nome da variável: API_SECRET  
valor da variável: 8%w8s%4#pa3(l8j95^%fr@d!^-gawt&ilg+=ackb==9e0iz4vn  

Nome da variável: PROFILE_ACTIVE
valor da variável: dev

Nome da variável: APP_URL_LOCAL  
valor da variável: <http://localhost:4200>  

Nome da variável: DATABASE  
valor da variável: apisistemavendas  

Nome da variável: DRIVER_POSTGRESQL  
valor da variável: org.postgresql.Driver  
  
Nome da variável: JAVA_HOME  
valor da variável: C:\Program Files\Java\jdk-11.0.11  

Nome da variável: PATH_TO_FX  
valor da variável: C:\Program Files\JavaFX\javafx-sdk-17.0.1  

Nome da variável: POSTGRES_PASSWORD  
valor da variável: postgres

Nome da variável: POSTGRES_URL  
valor da variável: jdbc:postgresql://localhost:5432/  

Nome da variável: POSTGRES_USER  
valor da variável: postgres  

Nome da variável: SPRING_APPLICATION_NAME  
valor da variável: Spring-Boot-Api  
