# Spring Security

## [Video tutorial](https://www.youtube.com/watch?v=her_7pa0vrg)

### Habilitando

- Habilitar o Spring Security no projeto

````XML
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
````

- Anotações e configurações em classe java.
- Sujestão de pacote config.security SecurityConfigurations

- Marcar classe com a anotação @EnableWebSecurity e @Configuration, extender WebSecurityConfigureAdapter

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {
}
````

- Spring security já esta habilitado.

### Configurando

- implementar os três métodos que herdamos da classe ***WebSecurityConfigureAdapter***.

```JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {

  @Override
  protected void configure (AuthenticationManagerBuilder auth) throws Exception {}

  @Override
  protected void configure (HttpSecurity http) throws Exception {}

  @Override
  protected void configure (WebSecurity web) throws Exception {}
}
```

- No primeiro método que recebe o parametro auth, é usado para configuração de autenticação.
- No Segundo método que recebe o parametro http, é usado para configurações de autorização(url, perfils de acesso).
- No Terceiro método que recebe o parametro web,é usado para configurações de recursos estáticos(thimeleaf, css, js, imagens)

### Configurando acesso a URL's

- Liberando acesso a métodos GET

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {
  @Override
  protected void configure (HttpSecurity http) throws Exception {
    http
        .authorizeRequests() 
        .antMatches(HttpMethod.GET, "/endpoint").permitAll()
        .antMatches(HttpMethod.GET, "/endpoint/*").permitAll();
  }
}
````

- Dessa forma, os endpoints ___"/endpoint"___ passado por parametro ficam publicos.

### Bloqueando acesso a outros endpoints

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {
  @Override
  protected void configure (HttpSecurity http) throws Exception {
    http
        .authorizeRequests() 
        .antMatches(HttpMethod.GET, "/endpoint").permitAll()
        .antMatches(HttpMethod.GET, "/endpoint/*").permitAll()
        .anyRequest().authenticated();
  }
}
````

- Apenas requisições autenticadas serão permitidas.

## Definindo lógica de autenticação

- Criar um dóminio que irá representar o usuario logado.
- No determinado dominio que representa o usuario no sistema, implementar a interface UserDetails

````JAVA
@Entity
public class Usuario implements UserDetails {
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String nome;
  private String email;
  private String senha;

  @ManyToMany(fetch = FetchType.EAGER) // para forçar o carregamento de todas as grants
  private List<Perfil> perfis = new ArrayList<>();
  // Construtores
  // Getters and Setters
}
````

### Implementar os vários método que UserDetail exige

- getAuthorities() - retorna uma collection de perfis de acesso()
  - Criar uma classe de Perfil e implementar a classe GrantedAuthority.

````JAVA
@Entity
public class Perfil implements GrantedAuthority {

  @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String nome; // nome do perfil

  @Override
  public String getAuthority(){
    return nome;
  }

  //Contrutores
  //Getter and Setter
}
````

- getPassword() - retorna a senha
- getUserName() - retorna o nome de usuario

### Métodos que ajudam no controle(se não possuir uma lógica, retornar todas como true)

- isAccountNonExpired() - retorna se a conta esta expirada
- isAccountNonLocked() - retorna se a conta esta bloqueada
- isCredentialsNonExpired() - retorna se as credenciais estão expiradas
- isEnabled() - retorna se a conta está ativa

### Pedindo para o Spring gerar um formulário de login

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {
  @Override
  protected void configure (HttpSecurity http) throws Exception {
    http
        .authorizeRequests() 
        .antMatches(HttpMethod.GET, "/endpoint").permitAll()
        .antMatches(HttpMethod.GET, "/endpoint/*").permitAll()
        .anyRequest().authenticated();
        .and()
        .formLogin(); // Spring criará uma pagina de login básica
  }
}
````

## Autenticando o Usuário

- No método que recebe auth como parametro.

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {
@Override
  protected void configure (AuthenticationManagerBuilder auth) throws Exception {
    auth
      .userDetailService(autenticacaoService) // classe onde fica a lógica de autenticação
  }
}
````

- Criar a classe AutenticacaoService, no pacote security
- Implementar a interface UserDetaislService

````JAVA
@Service
public class AutenticacaoService implements UserDetailsService {

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{}
}
````

- Criar um Repository para o Usuario

````JAVA
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
  Optional<Usuario> findByEmail(String email);
}
````

- Usar o repository para buscar o usuario no banco de dados

````JAVA
@Service
public class AutenticacaoService implements UserDetailsService {

  @Autowired
  private UsuarioRepository repository;
  
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
    Optional<Usuario> usuario = repository.findByEmail(username);
    if(usuario.isPresent()){
      return usuario.get();
    }
    throw new UsernameNotFoundException("Dados inválidos);
  }
}
````

- Injetar o servico de autenticacao na classe SecurityConfigurations e passar no argumento de userDetailsService.

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {

  @Autowired
  private AutenticacaoService autenticacaoService;

  @Override
  protected void configure (AuthenticationManagerBuilder auth) throws Exception {
    auth
      .userDetailService(autenticacaoService) // classe onde fica a lógica de autenticação
      .passwordEncoder(new BCryptPasswordEncoder()) // passando a forma de encoder 
  }
}
````

### Gerando uma senha com BCrypt

- Rodar o seguinte metodo para conseguir uma senha

````JAVA
public static void main(String[] args){
  System.out.println(New BCryptPAsswordEncoder().encode("123456"));
}
````

## Gerando Token com JWT

- É uma boa pratica usar JWT para API's, pois a api nao deve guardar estado.
- Seguir todos os passos exibidos [aqui](./spring-security.md)
- Adicionar a dependencia do JJWT no projeto

````XML
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt</artifactId>
    <version>0.9.1</version>
</dependency>
````

- Configurar a parte de segurança

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {

  @AutoWired
  private AutenticacaoService autenticacaoService;

  @Override
  @Bean
  protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }

  @Override
  protected void configure (HttpSecurity http) throws Exception {
    http
        .authorizeRequests() 
        .antMatches(HttpMethod.GET, "/endpoint").permitAll()
        .antMatches(HttpMethod.GET, "/endpoint/*").permitAll()
        .antMatches(HttpMethod.POST, "/auth").permitAll()
        .anyRequest().authenticated();
        .and().csrf().disable()
        .sessionManagement()
        .sessionCreationPolice(SessionCreationPolicy.STATELESS);
  }
}
````

- Criar um controller para receber a lógica de autenticação
- Injetar a classe AutenticationManager no controller

````JAVA
@RestController
@RequestMapping("/auth")
public class AutenticacaoController {

  @AutoWired
  private AutenticationManager autenticationManager;

  @AutoWired
  private TokenService tokenService;

  @PostMapping
  public ResponseEntity<?> autenticar(@RequestBody @Valid UsuarioDto LoginForm form){
    UserNamePasswordAuthenticationToken dadosLogin = form.converter();
    
    try{
      Authentication authentication = autenticationManager.authenticate(dadosLogin);
      String token = tokenService.gerarToken(autenticationManager);
      return ResponseEntity.ok().build();
    }catch(AuthenticationException e){
      return ResponseEntity.badRequest().build();
    }
  }
}
````

- Criar um DTO para os dados do LoginForm

````JAVA
public class LoginForm{
  private String email;
  private String senha;

  // Contrutores

  public UserNamePasswordAuthenticationToken converter(){
    return new UserNamePasswordAuthenticationToken(email, senha);
  }

  // Getters and Setters
}
````

- Criar a classe TokenService para criar o token

````JAVA
@Service
public class TokenService {

  @Value("${forum.jwt.expiration}")
  private String expiration;

  @Value("${forum.jwt.secret}")
  private String secret;

  public String gerarToken(Authentication authentication){
    Usuario logado = (Usuario) authentication.getPrincipal();
    Date hoje = new Date();
    Date dataExpiracao = new Date(hoje.getTime() + Long.parseLong(expiration))
    return Jwts.builder()
                    .setIssuer("{{NOME DA API QUE GEROU O TOKEN}}")
                    .setSubject(logado.getId().toString())
                    .setIssuedAt(hoje)
                    .setexpiration(dataExpiracao)
                    .singWith(SignatureAlgorithm.HS256, secret)
                    .compact();
  }

  public Boolean isTokenValido(String token){
    try{
      Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
      return true;
    }catch (Exception e){
      return false;
    }
  }

  public Long getIdUsuario(String token){
    Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
    return Long.parseLong(claims.getSubject());
  }
}
````

- Criar duas propriedade no application.properties, uma para armazenar o secret(uma string aleatoria) e o expiration(guarda um numero inteiro que representa a quantidade de tempo em milesegundo)

````YAML
forum.jwt.secret=rm'!@N=Ke!~p8VTA2ZRK~nMDQX5Uvm!m'D&]{@Vr?G;2?XhbC:Qa#9#eMLN\}x3?JR3.2zr~v)gYF^8\:8>:XfB:Ww75N/emt9Yj[bQMNCWwW\J?N,nvH.<2\.r~w]*e~vgak)X"v8H`MH/7"2E`,^k@n<vE-wD3g9JWPy;CrY*.Kd2_D])=><D?YhBaSua5hW%{2]_FVXzb9`8FH^b[X3jzVER&:jw2<=c38=>L/zBq`}C6tT*cCSVC^c]-L}&/

forum.jwt.expiration=86400000
````

- Criar um DTO para o Token

````JAVA
public class TokenDto {
  
  private String token;
  private String tipo;

  public Token(String token, String tipo){
    this.token = token;
    this.tipo = tipo;
  }

  //Getters
}


@RestController
@RequestMapping("/auth")
public class AutenticacaoController {

  @AutoWired
  private AutenticationManager autenticationManager;

  @AutoWired
  private TokenService tokenService;

  @PostMapping
  public ResponseEntity<TokenDto> autenticar(@RequestBody @Valid UsuarioDto LoginForm form){
    UserNamePasswordAuthenticationToken dadosLogin = form.converter();
    
    try{
      Authentication authentication = autenticationManager.authenticate(dadosLogin);
      String token = tokenService.gerarToken(autenticationManager);
      return ResponseEntity.ok(new TokenDto(token, "Bearer"));
    }catch(AuthenticationException e){
      return ResponseEntity.badRequest().build();
    }
  }
}
````

- Criar um filtro para pegar o token e validar
- no pacote security
- injetar na classe SecurityConfigurations

````JAVA

public class AutenticacaoViaTokenFilter extends OncePerRequestFilter{

  private TokenService tokenService;
  private UsuarioRepository repository;

  public AutenticacaoViaTokenFilter(TokenService tokenService, UsuarioRepository repository) {
    this.tokenService=tokenService;
    this.repository=repository;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
    String token = recuperarToken(request);
    Boolean valido = tokenService.isTokenValido(token);
    if(valido){
      autenticarCliente(token);
    }
    chain.doFilter(request, response);
  }

  private String recuperarToken(HttpServletRequest request){
    String token = request.getHeader("Authorization");
    if(token == null || token.isEmpty() || !token.startsWith("Bearer ")){
      return null;
    }
    return token.substring(7, token.length());
  }

  private void autenticarCliente(String token){
    Long idUsuario = tokenService.getIdUsuario(token);
    Usuario = repository.findById(idUsuario).get();
    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities())
    SecurityContextHolder.getContext().setAuthentication(authentication);
  }
}
````

````JAVA
@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigureAdapter {

  @AutoWired
  private AutenticacaoService autenticacaoService;

  @AutoWired
  private TokenService tokenService;

  @Autowired
  private UsuarioRepository usuarioRepository;

  @Override
  @Bean
  protected AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }

  @Override
  protected void configure (HttpSecurity http) throws Exception {
    http
        .authorizeRequests() 
        .antMatches(HttpMethod.GET, "/endpoint").permitAll()
        .antMatches(HttpMethod.GET, "/endpoint/*").permitAll()
        .antMatches(HttpMethod.POST, "/auth").permitAll()
        .anyRequest().authenticated();
        .and().csrf().disable()
        .sessionManagement()
        .sessionCreationPolice(SessionCreationPolicy.STATELESS)
        .and()
        .addFilterBefore(new AutenticacaoViaTokenFilter(tokenService, usuarioRepository), UserNamePasswordAuthenticationFilter.class);
  }
}
````

- Necessário sempre passar o tipo e o token no cabeçalho de todas as requisições.

[voltar](../README.md)
