# Manutenção Windows e melhorias

#### [MATERIAL COMPLETO EM VÍDEO](https://drive.google.com/drive/folders/1ZN4w1X7Nb9C-jqFb-J3Xnw-LTFYY3lNy?usp=sharing)
---
# Passo 1 - Análise inicial do disco rígido e do windows

Tutorial em vídeo [aqui](https://drive.google.com/file/d/1btbZq8EpCwXDJKviSqdS7EkTr1vGvVcW/view?usp=sharing)

Executar no **powershell** como administrador.

Verifica se o disco rígido está funcionando perfeitamente. ~20min a 1h.

``` Powershell
PS> chkdsk /r
```

Após executar o comando, é necessário reiniciar o PC.

Verifica e repara arquivos protegidos do windows. ~20min

``` Powershell
PS> sfc /scannow
```

Repara arquivos corrompidos ou ausentes do sistema operacional. ~30min

``` Powershell
PS> dism /online /cleanup-image /restorehealth
``` 
---
# Passo 2 - Eliminação de arquivos temporários

Tutorial em vídeo [aqui](https://drive.google.com/file/d/16odNkqp_ef7sV0O-xLuMlG35DOHJr-Bb/view?usp=sharing)

Executar a **Limpeza de disco** do próprio windows, botão direito no disco, propriedades, limpeza de disco.

Baixar e Executar o CCLeaner

- [Download CCleaner](https://www.ccleaner.com/pt-br/ccleaner/download/standard)

---
# Passo 3 - Remoção de Bloatwares

Tutorial em vídeo [aqui](https://drive.google.com/file/d/1dK6-o5Br6S3NWxQMLWKFYycgtLZAb628/view?usp=sharing)

- Remover todos os softwares de fabricantes que venham pré-instalados com o windows.
  - Usar o [Revo Uninstaller](https://www.revouninstaller.com/start-freeware-download) 
---
# Passo 4 - Remover PUP's

Tutorial em vídeo [aqui](https://drive.google.com/file/d/1zKbgFuuYss-5SbhU2xQEOs6dA7vaT3Rc/view?usp=sharing)

- Remover PUP's (programas que são instalados junto de outros softwares) usando o [Adwarecleaner](https://downloads.malwarebytes.com/file/adwcleaner)
- Desinstalar qualquer outro programa de otimização e limpeza exceto CCLeaner usando o Revo Uninstaller.
---
# Passo 5 - Eliminação tripla de Malwares
Tutorial em vídeo [aqui](https://drive.google.com/file/d/11YfavZT3TQ4pk4XlzAb8u7I0zmC-pcT4/view?usp=sharing)

- [Panda Cloud Clenaer](https://www.pandasecurity.com/en/homeusers/cloud-cleaner/)
- [TREND Micro](https://api.link.trendmicro.com/events/landing-page?product_id=9cdd24e918fa9869266a7905d8594fcd8838fb6ebe86b6397d6ae320&source=experience)
- [Eset Online Scanner](https://download.eset.com/com/eset/tools/online_scanner/latest/esetonlinescanner.exe)
---

# Passo 6 - Antivírus

Tutorial em vídeo [aqui](https://drive.google.com/file/d/1lQ3v6uoQcQKiFJe7PGiwCoTTutn_eB0r/view?usp=sharing)

Evitar os seguintes antivírus

- avast
- AVG
- COMODO
- McAfee
- Baidu Antivirus
- 360 Total Security
- Tecent
- PSafe

Antivirus Indicados

- Kaspersky
- panda
- Bitdefender
- Avira
- TREND MICRO
- F-Secure
- ESET

Melhores escolhas

Opções free

- [Panda](https://www.pandasecurity.com/en/homeusers/free-antivirus/)
- [Configuração do Panda Dome]()

Opção paga

- [Kaspersky](https://www.kaspersky.com.br/total-security)
---
# Passo 7 - Bloqueador de propagandas + Malvertising

Tutorial em vídeo [aqui](https://drive.google.com/file/d/179HuVvXANEVnNONp4eOmhWfIqtBn5a0R/view?usp=sharing)

- [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?utm_source=chrome-ntp-icon)

# Passo 8 - Bloqueador de URL + Phising

Tutorial em vídeo [aqui](https://drive.google.com/file/d/1TXgDEqj96j5IW83AIcwelgFDDAzQdUdW/view?usp=sharing)

- [TrafficLight](https://chrome.google.com/webstore/detail/trafficlight/cfnpidifppmenkapgihekkeednfoenal?utm_source=chrome-ntp-icon)
---
# Passo 9 - Desfragmentação no windows
Tutorial em vídeo [aqui](https://drive.google.com/file/d/1w5CCIuM-KycjEbtj04HtDpOKZIeI3eFs/view?usp=sharing)

Baixar o Defreggler

- [Defraggler](https://www.ccleaner.com/defraggler?cc-noredirect=)

Desfragmentação em SSD

- [Otimização de SSD A FUNDO](https://www.youtube.com/playlist?list=PLQfECKnJK6Rypprmz7_mdIVHfRFCQOdIV)
---
# Passo 10 - Windows update

Tutorial em vídeo [aqui](https://drive.google.com/file/d/1cMk2p0cd65s6ZcU9IY_M3p5rWjwssm5k/view?usp=sharing)

---
# Passo 11 - 10 Dicas

- Mantenha sempre o windows atualizado
- Mantenha sempre o antivírus atualizado
- Cuidado com notícias e imagens chamativas na web
- A cada 15 dias executar o CCleaner e o Defraggler
- Cuidado com extensões do chrome
- Não acredite em tudo que recebe por email ou que lê na web
- Sempre use programas atualizados e na ultima versão
- Nao instale nenhum programa que prometa milagres. [veja aqui](https://www.baboo.com.br/windows-10/conteudo-essencial-windows/praga-dos-otimizadores-de-pc/)
- Não use crakes ou ativadores.
---