# OCI Architecture

## Regions

- Região é uma área geográfica localizada que compreende um ou mais dominios de disponibilidade
- Dominios de disponibilidade
- Fornece proteção contra falhas inteiras de dominio de disponibilidade em uma região com vários ADs

## Availability Domains(AD)

- Dominio de disponibilidade são um ou mais data centers tolerantes a falhas(FD) localizados em uma região conectados entre si por uma rede de largura de banda alta e de baixa latência

## Fault Domains(FD)

- Dominios de falha são um agrupamento de hardware e infraestrutura dentro de um dominio de dispnibilidade(AD), data centers lógicos, 3 por região

## Choosing a Region

- Location - Mais próximo possivel dos usuarios
- Data Residency & Compliance - Armazenamento de dados conforme LGPD
- Service Availability - Disponibilidade de serviços necessários

## High Availability Desing

## Fault Domains(DF)

- Dominos de falha são 3 por região, oferecendo proteção contra falhas dentro de um domínio de disponibilidade.

## Region Pair

- Na maioria dos paises existem pelo menos dois data centers, sempre usar o segundo datacenter para fazer backups de recuperação ou ajudar a cumprir os requisitos de residência(LGPD)

___

## Create a Oracle Cloud Free Tier Account

- Será disponibilizado $300 de créditos para uso dos serviços ou 30 dias, o que vier primeiro encerra o periodo free
- [Oracle Cloud Free Tier](https://oracle.com/cloud/free/)

## Setting up your Tenancy

- Tenancy ou Root Compartment - Diretorio raiz
- Usado para definir os Usuarios, Grupos e Politicas
- O compartimento raiz ou tenancy pode conter todos os recursos de nuvem, recursos de computação, armazenamento, banco de dados, redes, tudo dentro desse compartimento raiz.
- 1° Best practice: Criar compartimentos dedicados para isolar cada recurso.
- Administradores devem ter outros administradores para cada compartimento a fim de administrar o recurso.
- 2° Best practice: Não usar a conta TENANCY ADMIN para gerenciar os recursos, sempre delegar essa atividade
- 3° Best practice: Impor os usuarios a usarem autenticação multifator MFA

## Config

- Identity & Security > Onde posso criar Usuarios, Admins, Compartimentos, definir Politicas

___

## Identity and Access Management

- IAM = Identity and Access Management(Gerenciamento de identidade e acesso)
- Fine-grained Access Controle = Controle de acesso refinado, controla quem pode acessar determinado recurso, também conhecido como controle de acesso baseado em função, que é a mesma coisa que usar privilégios, especificando privilégios para funções específicas
- AuthN - Who are you? = Verifica que você é a pessoa ou o recurso que você é.
  - Username/Password
  - API Singing Keys
  - Authentication token
- AuthZ - What permissions do you have? = Verifica quais as permissões a pessoa ou recurso tem.
  - Policies

## OCI Identity Concepts

## Users

- Podem ser seres humanos ou serviços especificos ou instâncias de computação específicas

## Groups

- Você pode agrupar usuarios em grupos de usuários que precisam do mesmo tipo de permisão, um grupo pode ser especifico para ums serviço, administradores de algum serviço, armazenamento, rede ou de geografia.

## Polices

- Definem que tipo de permissão os usuarios de um grupo vão ter, e são escritas nos compartimentos

## Federation

## Network Sources

## Principals

- É uma entidade do IAM que tem permissão para interagir com recursos da OCI

## Dynamic Groups

## Compartments

- Onde ficam a relação de recursos
- Best practice - Criar compartimentos para isolar recursos
- Grupos + Polices = Acesso ao compartimento
- Seis niveis de compartimento

## Resource

- São os objetos da cloud, qualquer coisa que você pode criar é um recurso
- São identificados por um OCIP. identificador unico
**ocid1.RESOURCE TYPE.REALM.REGION.FUTURE USE.UNIQUE ID**

[voltar](../README.md)
