# OCI

## 1 - INFRASTRUCTURE

- Compute, Containers, OS, Vmwares - executar calculos, logica e aplicações.
- Storage - Fornece uma gama completa de serviços de armazenamento (Store, Access, Govern, Analyze)
- Network - Permite configurar redes privadas, e fornece o mais amplo e profundo serviço de rede.(Reliability, Security Features, Performance)
  
### Compute

- Bare metal
- VM
- CPUs
- GPUs
- HPC
  
### Containers

- Containers
- Kubernetes
- Service Mesh
- Registry
  
### OS, Vmware

- Autonomous Linux
- OS Mgmt Service
- Marktplace
  
### Storage

- NVMe
- Block
- File
- Object
- Archive
- Data Transfer
  
### Networking

#### VCN

- Na sua exxência, é uma rede privada definida por software criada no Oracle Cloud, usada pra uma comunicação segura entre instâncias que se comunicãm entre si, independente da região, altamente disponivel, e escalável.
- CIDR(Roeteamento de intermdomínio sem classe)
- A VCN tem endereços de ip e um range, esse range pode ser dividido em sub redes menores para receber instâncias diferentes de recursos.
- A comunicação entre os recursos e a internet e feita através de um gateway obrigatório, todas as conexões entre a VCN e a internet deve passar por esse gateway
- O Internet Gateway é altamente disponível e altamente escalável.
- Um servidor NAT tbm é disponibiliado junto com o Gateway, e o tráfego e sempre unidirecional, a idéia desse gateway é permitir todas as conexões vindo de dentro do vcn e bloquear todas as que partem da internet
- Service Gateway é usado para permitir acessos de recursos internos a serviços publicos do OCI sem precisar passar por um gateway de internet ou NAT

#### LB

- LB

#### Service Gateway

- Service Gateway
  - Service Gateway é usado para permitir acessos de recursos internos a serviços publicos do OCI sem precisar passar por um gateway de internet ou NAT
- FC
- VPN
- Cluster Networking

## 2 - DATABASES

- Databases - Executa bases oracle e bases open source (OLTP, OLAP, JSON)
- Distributed & OSS Databases - Executa bases NoSQL e MySQL

### Oracle Databases

- ATP
- ADW
- DBCS
- VM/BM
- JSON
- Dedicated,
- Exadata
- Exadata C@C

### Distributed & OSS Databases

- NoSQL
- MySQL

___

## 3 - DATA & AI

- Big Data - Gerenciamento de dados usando apache spark
- Treinamento e aprendizado de máquina
- Messaging - Serviço de stream de mensagens

### Big Data

- Big Data
- Data Flow
- Data Integration
- Data Catalog
- Golden Gate

### AL Services

- Data Science
- Digital Assistant

### Messaging

- Streaming
- Queueing
- Service
- Connector

___

## 4 - GOVERNACE & ADMINSTRATION

- Cloud Ops - Gerenciamento de ambientes grandes e complexos
- Security - Gerenciamento de segurança, proteção de rede robusta pu criptografia padrão
- Observability - Plataforma de registros de log, Monitoramento e gerenciamento de desempenho

### Cloud Ops

- IAM
- Compartments
- Tagging
- Console
- Cost Advisor

### Security

- Cloud Guard
- Security Zones
- Vault
- KMS
- Data Safe
- DDoS
- WAF

### Observability

- Monitoring
- Logging
- Loggind Analytics
- Notifications
- Events
- Operations Insigthts
- APM
- MAnagement Cloud
  
___

## 5 - DEVELOPER SERVICES

- Low Code
- AppDev - Serviços para desenvolvedores
- Infrastructure as Code
  
### Low Code

- APEX

### AppDev

- Visual Builder Studio
- GraalVM
- Helidon
- SQL
- Developer
- Shell
- APIs/CLI/SDKs/Docs

### Infrastructure as Code

- Resource Manager
- Terraform
- Ansible

___

## 6 - ANALICTCS

- Business Analytics - Integra varias soluções de terceiros para analises

### Business Analitics

- Analitcs Cloud
- Fusion Analytcs

___

## 7 - APPLICATIONS

- Serverless - Serviços para ajudar a criar microserviços
- App Integration - Integrações
- Business & Industru SaaS - Abrangente pacote de recursos humanos, cadeia de suprimentos, manufatura, publicidade e vendas entre outros.

### Serverless

- Events
- Functions
- API Gateway

### App Integration

- Integration Cloud
- Workflow
- Notifications
- Email Delivery

### Business & Industru SaaS

- ERP
- HCM
- SCM
- Sales
- Marketing
- Service
- Vertical Industry

___

[voltar](../README.md)