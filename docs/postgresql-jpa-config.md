# Configurações application.properties

## banner

spring.main.banner-mode=off

## PostgreSQL

spring.datasource.url=jdbc:postgresql://localhost:5432/mudi
spring.datasource.username=postgres
spring.datasource.password=postgres
spring.datasource.driver-class-name=org.postgresql.Driver

## drop n create table again, good for testing, comment this in production

spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.show-sql=true

## HikariCP settings

spring.datasource.hikari.connection-timeout=20000
spring.datasource.hikari.maximum-pool-size=5

## logging

logging.pattern.console=%d{yyyy-MM-dd HH:mm:ss} %-5level %logger{36} - %msg%n
logging.level.org.hibernate.SQL=debug

logging.level.org.hibernate.type.descriptor.sql=trace
logging.level.root=error

[voltar](../README.md)