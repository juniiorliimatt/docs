# Ambiente de desenvolvimento WSL2

## Diretamente pelo powershell como administrador

```Powershell
PS> wsl --install
```

### Escolha sua distribuição Linux no Windows Store

Escolha sua distribuição Linux preferida no aplicativo Windows Store, sugerimos o Ubuntu por ser uma distribuição popular e que já vem com várias ferramentas instaladas por padrão.

![Distribuições Linux no Windows Store](./img/distribuicoes_linux.png)

Ao iniciar o Linux instalado, você deverá criar um **nome de usuário** que poderá ser o mesmo da sua máquina e uma **senha**, este será o usuário **root da sua instância WSL**.

Parabéns, seu WSL2 já está funcionando:

![Exemplo de WSL2 funcionando](./img/wsl-funcionando.png)

## Após instalação criar usuário e definir senha, para isso apenas abrir o bash do ubuntu

### Verificando distribuições instaladas no powershell

```powershell
wsl -l -v
```

![distribuições instaladas](./img/verificando_distribuicoes_instaladas_do_linux_no_wsl2.png)

### (Opcional) Desinstale o Hyper-V

Agora que temos o WSL 2 não precisamos mais do Hyper-V, desabilite-o em Painel de Controle > Programas e Recursos (se você tiver instalado o Hyper-V).

### (Opcional) Usar Windows Terminal como terminal padrão de desenvolvimento para Windows

Uma deficiência que o Windows sempre teve era prover um terminal adequado para desenvolvimento. Agora temos o **Windows Terminal** construído pela própria Microsoft que permite rodar terminais em abas, alterar cores e temas, configurar atalhos e muito mais.

Instale-o pelo Windows Store e use estas [configurações padrões](windows-terminal-settings.json) para habilitar WSL 2, Git Bash e o tema drácula e alguns atalhos.

Para sobrescrever as configurações **clique a seta para baixo do lado das abas e em configurações**, abrirá as configurações do Windows Terminal, apenas cole o conteúdo do arquivo JSON e salve.

## O que o WSL 2 pode usar de recursos da sua máquina

Podemos dizer que o WSL 2 tem acesso quase que total ao recursos de sua máquina. Ele tem acesso por padrão:

* A todo disco rígido.
* A usar completamente os recursos de processamento.
* A usar 80% da memória RAM disponível.
* A usar 25% da memória disponível para SWAP.

Isto pode não ser interessante, uma vez que o WSL 2 pode usar praticamente todos os recursos de sua máquina, mas podemos configurar limites.

Crie um arquivo chamado `.wslconfig` na raiz da sua pasta de usuário `(C:\Users\<seu_usuario>)` e defina estas configurações:

```txt
[wsl2]
memory=8GB
processors=4
swap=2GB
```

Estes são limites de exemplo e as configurações mais básicas a serem utilizadas, configure-os às suas disponibilidades.
Para mais detalhes veja esta documentação da Microsoft: [https://docs.microsoft.com/pt-br/windows/wsl/wsl-config#wsl-2-settings](https://docs.microsoft.com/pt-br/windows/wsl/wsl-config#wsl-2-settings).

Para aplicar estas configurações é necessário reiniciar as distribuições Linux, então sugerimos executar no PowerShell o comando: `wsl --shutdown` (Este comando vai desligar todas as instâncias WSL 2 ativas e basta abrir o terminal novamente para usa-la já com as novas configurações).

## Como acessar o WSL pelo explorer

* Digitar \\wsl$ na barra do explorer
  
![Acessando wsl via explorer](./img/acessando_wsl2_no_explorer.png)

## Como acessar discos locais pelo WSL

```bash
# dentro da pasta /mnt/
cd /mnt/
```

![/mnt/](./img/mount_no_wsl2.png)

## 4 - Uma vez definida o usuário e a senha pode começar o processo de atualização e instalação de pacotes

## Atualizar a distribuição
  
```bash
sudo apt update && sudo apt upgrade -y
```

## Instalar o ZSH

```bash
sudo apt install zsh
```

## Instalar GIT

```bash
sudo add-apt-repository ppa:git-core/ppa
sudo apt update -y
sudo apt install git -y
```

## Criar chave SSH

```bash
# criar chave ssh em ~/.ssh
$ ssh-keygen -t ed25519 -C "<email ou usuário>"

# Generating public/private ed25519 key pair.
# Enter file in which to save the key (/home/user/.ssh/id_ed25519): <ENTER>

# Enter passphrase (empty for no passphrase):
# Enter same passphrase again:<ENTER>

$ eval $(ssh-agent -s)
$ ssh-add ~/.ssh/<KEY>

# Save these settings in the ~/.ssh/config file.
# GitLab.com
# Host gitlab.com
#   PreferredAuthentications publickey
#   IdentityFile ~/.ssh/gitlab_com_rsa

# Private GitLab instance
# Host gitlab.company.com
#   PreferredAuthentications publickey
#   IdentityFile ~/.ssh/example_com_rsa

# Copiar e colar a chave SSH no github ou gitlab
cat ~/.ssh/id_ed25519.pub

# Verificar se deu certo
# Substitua gitlab.example.com pelo serviço remoto ex: gitlab.com ou github.com ou o serviço de git da empresa.
ssh -T git@gitlab.example.com 
```

## Instalar o Oh-my-zsh

```bash
# Ir para a pasta home do usuário cd ~
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# adicionar o zinit
sh -c "$(curl -fsSL https://git.io/get-zi)" -- -a zunit

# Setup ZI directory
zi_home="${HOME}/.zi" && mkdir -p $zi_home

# Clone repository
git clone https://github.com/z-shell/zi.git "${zi_home}/bin"

# Source zi.zsh in your .zshrc from previously created directory:

zi_home="${HOME}/.zi"
source "${zi_home}/bin/zi.zsh"

# Enable completions:
# INFO
# The next two lines must be below the above two:

autoload -Uz _zi
(( ${+_comps} )) && _comps[zi]=_zi

# adicionar spacecship
git clone git@github.com:spaceship-prompt/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" --depth=1

````
## Buscar configurações do [.zshrc](configuracoes/.zshrc)  


````bash
# adicionando link do tema
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"

# Set ZSH_THEME="spaceship" in your .zshrc

# adicionar fast-syntax
git clone https://github.com/zdharma-continuum/fast-syntax-highlighting ~/.local/share/zinit/plugins/fast-syntax-highlighting/

# adicionar ao .zshrc
source ~/.local/share/zinit/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
```

## Instalar o JDK

### [JDK 11 - Baixar o arquivo .tar.gz](https://www.oracle.com/java/technologies/downloads/#java11)  

### [JDK 17 - Baixar o arquivo .tar.gz](https://www.oracle.com/java/technologies/downloads/#java17)

````bash
# criar a pasta jvm em /usr/lib/
sudo mkdir /usr/lib/jvm

# descompactar o jdk 
sudo tar -xvzf jdk-11.0.14_linux-arch64_bin.tar.gz

# mover o jdk descompactado para /usr/lib/jvm
sudo mv jdk-11.0.14 /usr/lib/jvm

# inserir o path no .bashrc ou .zshrc
export JAVA_HOME=/usr/lib/jvm/jdk-11.0.14
export PATH=$JAVA_HOME/bin

# abrir o path
# sudo code /etc/environment

# inserir depois de PATH=
# /usr/lib/jvm/jdk-11.0.14/bin:

# Atualizar o update-alternatives

sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk-11.0.14/bin/java" 0

sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk-11.0.14/bin/javac" 0

sudo update-alternatives --set java /usr/lib/jvm/jdk-11.0.14/bin/java

sudo update-alternatives --set javac /usr/lib/jvm/jdk-11.0.14/bin/javac

update-alternatives --list java

java -version

# Os passos acima podem ser aplicados ao JDK 17, apenas se atentando de mudar a versão java e nome de pastas nos comandos
````

## Instalar Node LTS

````bash
# Using Ubuntu
curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt-get install -y nodejs
````

## Instalar Git-Flow
  
```bash
sudo apt install git-flow -y
```

## Instalar Curl

```bash
sudo apt install curl -y
```

## Instalar o Unrar

```bash
sudo apt install unrar
```

## Instalar o Unzip

```bash
sudo apt install unzip -y
```

## Instalar o Xclip

```bash
sudo apt install xclip -y
```

## Instalar o Maven

```bash
# Baixar o maven com o wget
wget https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip

# Baixar o maven com o curl
$ curl --location --show-error -O --url https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip

# criar um diretório em /opt
$ sudo mkdir /opt/maven
# descompactar o maven no diretório criado
$ sudo unzip -d /opt/maven apache-maven-3.8.4-bin.zip

# verificar se o maven foi extraído corretamente
$ ls /opt/maven/maven apache-maven-3.8.4
LICENSE  NOTICE  README.txt  bin  boot  conf  lib

# exportar a variável de ambiente dentro do arquivo .bashrc ou .zshrc
export M3_HOME=/opt/maven
export PATH=$M3_HOME/bin:$PATH
```

## Instalar o Gradle

```bash

# Baixar o maven com o wget
$ wget https://services.gradle.org/distributions/gradle-7.4-bin.zip

# baixar o gradle usando curl
$ curl --location --show-error -O --url https://services.gradle.org/distributions/gradle-7.4-bin.zip

# criar um diretório em /opt
$ sudo mkdir /opt/gradle

# descompactar o maven no diretório criado
$ sudo unzip -d /opt/gradle gradle-7.4-bin.zip

# verificar se o maven foi extraído corretamente
$ ls /opt/gradle/gradle-7.4
LICENSE  NOTICE  bin  getting-started.html  init.d  lib  media

# exportar a variável de ambiente dentro do arquivo .bashrc ou .zshrc
export GRADLE_HOME=/opt/gradle/gradle-7.3.3
export PATH=$GRADLE_HOME/bin:$PATH
```

## Instalar Fonts Powerline

```bash
sudo apt-get install fonts-powerline -y
```

## Instalar o build-essential

```bash
sudo apt install build-essential -y
```

## Instalar o Docker com Docker Engine (Docker Nativo)

A instalação do Docker no WSL 2 é idêntica a instalação do Docker em sua própria distribuição Linux, portanto se você tem o Ubuntu é igual ao Ubuntu, se é Fedora é igual ao Fedora. A documentação de instalação do Docker no Linux por distribuição está [aqui](https://docs.docker.com/engine/install/), mas vamos ver como instalar no Ubuntu.

Instale os pré-requisitos:

```bash
sudo apt update && sudo apt upgrade
sudo apt remove docker docker-engine docker.io containerd runc
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

Adicione o repositório do Docker na lista de sources do Ubuntu:

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

```bash
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Instale o Docker Engine

```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Dê permissão para rodar o Docker com seu usuário corrente:

```bash
sudo usermod -aG docker $USER
```

Instale o Docker Compose:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

Inicie o serviço do Docker:

```bash
sudo service docker start
```

Este comando acima terá que ser executado toda vez que Linux for reiniciado. Se caso o serviço do Docker não estiver executando, mostrará esta mensagem de erro:

```bash
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
```

Exemplo do docker funcionando dentro do wsl

![Exemplo do docker funcionando dentro do wsl](./img/docker_funcionando_dentro_do_wsl2.png)

## Se necessitar integrar o Docker com outras IDEs que não sejam o VSCode

O VSCode já se integra com o Docker no WSL desta forma através da extensão Remote WSL ou Remote Container.

É necessário habilitar a conexão ao servidor do Docker via TCP. Vamos aos passos:

* Crie o arquivo /etc/docker/daemon.json: `sudo echo '{"hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]}' > /etc/docker/daemon.json`
* Reinicie o Docker: `sudo service docker restart`

Após este procedimento, vá na sua IDE e para conectar ao Docker escolha a opção TCP Socket e coloque a URL `http://IP-DO-WSL:2375`. Seu IP do WSL pode ser encontrado com o comando `cat /etc/resolv.conf`.

Se caso não funcionar, reinicie o WSL com o comando `wsl --shutdown` e inicie o serviço do Docker novamente.

[voltar](../../README.md)
