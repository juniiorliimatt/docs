# Aplicativos para serem instalados após uma formatação

## Desenvolvimento

- [Java JDK](https://www.oracle.com/java/technologies/java-se-glance.html)
- [Java JDK 17, 11, 8](https://www.oracle.com/java/technologies/downloads/)
- [JavaFX](https://download2.gluonhq.com/openjfx/17.0.1/openjfx-17.0.1_windows-x64_bin-sdk.zip)
- [Scene Builder](https://gluonhq.com/products/scene-builder/thanks/?dl=https://download2.gluonhq.com/scenebuilder/17.0.0/install/windows/SceneBuilder-17.0.0.msi)
- [Maven](https://maven.apache.org/download.cgi)
- [Gradle](https://gradle.org/releases)
- [Node](https://nodejs.org/en/download/)
- [Angular](https://angular.io/guide/setup-local)
- [Intellij](https://www.jetbrains.com/idea/download/#section=windows)
- [Eclipse](https://www.eclipse.org/downloads/packages/)
- [Spring Tools Suite](https://download.springsource.com/release/STS4/4.13.0.RELEASE/dist/e4.22/spring-tool-suite-4-4.13.0.RELEASE-e4.22.0-win32.win32.x86_64.self-extracting.jar)
- [Lombok](https://projectlombok.org/downloads/lombok.jar)
- [Visual Studio Code](https://code.visualstudio.com/download)
- [NotePad++](https://notepad-plus-plus.org/downloads/)
- [PostgreSQL](https://www.postgresql.org/download/)
- [Oracle Express Edition](https://www.oracle.com/br/database/technologies/appdev/xe.html)
- [Oracle Database](https://www.oracle.com/database/technologies/oracle-database-software-downloads.html#19c)
- [Sql Developer](https://www.oracle.com/tools/downloads/sqldev-downloads.html)
- [Mysql Server](https://dev.mysql.com/downloads/)
- [Fira Code](https://github.com/tonsky/FiraCode)
- [JetBrains Mono](https://www.jetbrains.com/lp/mono/)
- [Monaco Nerd fonts](https://github.com/Karmenzind/monaco-nerd-fonts)
- [PowerShell](https://github.com/PowerShell/PowerShell)
- [Git](https://git-scm.com/downloads)
- [Postman](https://dl.pstmn.io/download/latest/win64)
- [Docker for Windows](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe?utm_source=docker&utm_medium=webreferral&utm_campaign=dd-smartbutton&utm_location=header)
- [AWS CLI](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/install-windows.html)
- [WSL](https://docs.microsoft.com/pt-br/windows/wsl/install)
- [Windows Sandbox](https://itonlineblog.wordpress.com/2021/07/05/guias-it-online-como-ativar-o-windows-sandbox-no-windows-11/)
- [Terraform](https://www.terraform.io/downloads)
- [Liquibase](https://www.liquibase.org/download)
- [Heroku](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)
- [DBeaver](https://dbeaver.io/files/dbeaver-ce-latest-x86_64-setup.exe)
- [Android Studio](https://developer.android.com/studio)
- [Chocolatey](https://chocolatey.org/install)
- [Open SSH](https://docs.microsoft.com/pt-br/windows-server/administration/openssh/openssh_overview)

## Trabalho

- [Fortinet VPN](https://www.fortinet.com/support/product-downloads#vpn)
- [Teams](https://www.microsoft.com/pt-br/microsoft-teams/download-app#desktopAppDownloadregion)

## Ferramentas úteis

- [Google Chrome](https://www.google.com/chrome/)
- [Cristal disk Info](https://crystalmark.info/en/download/#CrystalDiskInfo)
- [Cristal disk Mark](https://crystalmark.info/en/download/#CrystalDiskMark)
- [Discord](https://discord.com/api/downloads/distributions/app/installers/latest?channel=stable&platform=win&arch=x86)
- [Atube Catcher](https://www.atube.me/pt-br/)
- [Power Toys](https://github.com/microsoft/PowerToys/releases/)
- [Revo Unistall](https://www.revouninstaller.com/pt/start-freeware-download/)
- [Rufus](https://rufus.ie/pt_BR/)
- [Spotify](https://www.spotify.com/br/download/windows/)
- [VLC](https://get.videolan.org/vlc/3.0.16/win64/vlc-3.0.16-win64.exe)
- [7Zip](https://www.7-zip.org/download.html)
- [Kaspersky](https://www.kaspersky.com.br/home-security)
- [AnyDesk](https://anydesk.com/en/downloads/thank-you?dv=win_exe)
- [AMD Driver Video](https://www.amd.com/en/support/apu/amd-ryzen-processors/amd-ryzen-7-mobile-processors-radeon-rx-vega-graphics/amd-ryzen-1)
- [Whatsapp](https://web.whatsapp.com/desktop/windows/release/x64/WhatsAppSetup.exe)
- [Telegram](https://telegram.org/dl/desktop/win64)
- [Defraggler](https://www.ccleaner.com/defraggler?cc-noredirect=)
- [CCleaner](https://www.ccleaner.com/ccleaner/download)

## S145 Drivers

- [Drivers](https://pcsupport.lenovo.com/br/pt/products/laptops-and-netbooks/ideapad-s-series-netbooks/s145-15api/81v7/81v70009br/pe06xb3y)

## Games

- [Rockstar](https://www.rockstargames.com/downloads)
- [Battle.Net](https://www.blizzard.com/pt-br/apps/battle.net/desktop)  
- [Blizzard](https://www.blizzard.com/pt-br/download/)  
- [Steam](https://store.steampowered.com/about/)  
- [Epic Games](https://launcher-public-service-prod06.ol.epicgames.com/launcher/api/installer/download/EpicGamesLauncherInstaller.msi)

## Media Creation Tool

- [Windows 10](https://www.microsoft.com/pt-br/software-download/windows10)  
- [Windows 11](https://www.microsoft.com/en-us/software-download/windows11)  
- [Office Setup](https://setup.office.com/)  

[voltar](../README.md)
