# Configurações application.properties

## Datasource

- spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/alura
- spring.datasource.username=root
- spring.datasource.password=root
- spring.datasource.tomcat.test-while-idle=true
- spring.datasource.tomcat.validation-query=SELECT 1

## JPA

- spring.jpa.show-sql=true
- spring.jpa.hibernate.ddl-auto=update
- spring.jpa.hibernate.naming.physical-strategy=org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl
- spring.jpa.hibernate.naming.implicit-strategy=org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl

[voltar](../README.md)