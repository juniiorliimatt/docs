# Atalhos Eclipse

- [Post](https://medium.com/@francisco.kindermann/alguns-atalhos-do-eclipse-65ad47eec6ae#:~:text=O%20Eclipse%20IDE%20possui%20muitos%20atalhos%20que%20podem,verticalmente%2C%20sem%20a%20necessidade%20de%20copiar%20e%20colar.)
- Você sempre pode apertar **Ctrl+sShift+l** para ver todos os atalhos

## Alguns atalhos do Eclipse IDE

O Eclipse IDE possui muitos atalhos que podem ajudar o desenvolvedor na produtividade e com isso influenciar diretamente no tempo de desenvolvimento.
Vou exemplificar alguns atalhos que utilizo diariamente e que, ao meu ver, aumentam minha produtividade.

**Alt + Down/Up**  
Esse atalho desloca a linha verticalmente, sem a necessidade de copiar e colar.

**Ctrl + Alt + down/up**  
Duplica a linha onde se encontra o cursor para cima ou para baixo sem precisar selecionar. Obs: Alguns casos tem a necessidade de desligar esse atalho de girar a tela do App da Intel no Windows.

**Alt + Shift + C**  
Para alterar a assinatura do método basta clicar em cima do mesmo e acionar o atalho. De forma simples e segura você conseguirá modificar coisas como o nome, visibilidade, tipo de retorno, parâmetros, etc.

**Alt + Shift + A**  
Ao ativar é possível selecionar um bloco de código e modificar todas as linhas selecionadas simultaneamente. Para desativar basta precionar o atalho novamente.

**Alt + shift + I**  
Traz variáveis e métodos para linha em questão (Inline). Em variáveis o código em questão é removido, já em métodos tem a opção de manter o método na classe.

**Alt + Shift + R**  
Utilizo para renomear variáveis, métodos e classes. Após digitar o novo nome, caso eu apertar a tecla Enter o Eclipse irá alterar qualquer referencia externa a classe na qual realizei a renomeação. Mas caso aperto Esc somente irá renomear dentro da classe e qualquer referência irá continuar com o nome antigo e com alerta de erro. Se a necessidade é renomear uma variável que é usada somente na classe em questão, utilize o Esc, pois assim evitará um processamento desnecessário do Eclipse para buscar referência do mesmo em todos os projetos.

**Ctrl + Shift + X/Y**  
Esse atalho deixa a seleção maiúscula (X) ou minúscula (Y).

**Ctrl + Shift + G**  
Busca por referências da variável, método, classe, interface, etc…, na própria classe ou em outras classes do workspace.

**Ctrl + Shift + [**  
Duplica a edição da mesma classe em duas janelas.

**Ctrl + 1 (Invert equals)**  
Deixe o cursor em cima do equals para acionar o atalho e selecione Invert equals. Com isso irá inverter de forma rápida a comparação.
Ex: a.equals(b); > b.equals(a);

**Ctrl + 1 (Create ‘for’ loop)**  
Após a variável aperte a tecla de atalho e escolha uma das formas de loop (Por objeto, por índice ou por iterador). Pode ser usado após qualquer código que retorne uma lista.

**Ctrl + Q**  
Te levará para o ultimo local que houve qualquer edição. Muito útil para quando há muitas classes abertas.

**Ctrl + 3**  
Abre o Quick Access onde é possível pesquisar pelas diversas funcionalidades do Eclipse. Eu uso para chamar o generate getters and setters, digitando apenas “gett”, e generate constructor, digitando apenas “const”.

**Ctrl + K ou Ctrl + Shift + K**  
Busca pelo texto selecionado na classe (Mesma função do Ctrl + F). Basta selecionar e apertar a tecla de atalho. Com o shift ele retrocede na busca.

**Dois cliques (Para selecionar)**  
Utilizamos dois clique para selecionar uma palavra, porém quando feito ao lado dos caracteres ‘, “, (, {, [ (aspas simples, aspas duplas, parênteses, chaves e colchetes, ou seja, tudo que abre e fecha) o Eclipse seleciona o bloco inteiro, desde onde abre até onde fecha. É útil para selecionar a condição do if clicando no parentes, o corpo inteiro do método clicando na chave e o corpo da classe também clicando na chave. Também utilizo para saber a extensão dos blocos if/for e tamanho dos métodos de forma rápida, pois muitas vezes o código não está formatado e muito bagunçado.

Tamanho de caracteres no console.
Por padrão o Eclipse limita o tamanho de caracteres do console e muitas vezes acaba perdendo informação por causa desse limite.
Para remover o limite, clique com botão direito dentro do Console e clique em Preferences… e desmarque a opção Fixed width console

Working Set
Clicar na seta para baixo View menu na view Package Explorer e clicar em Select Working Set… Crie um novo Working Set e adicione os projetos que queira visualizar. Ainda sim será possível abrir as classes dos outros projetos.
