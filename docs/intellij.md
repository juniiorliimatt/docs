# Download

## Instalar o ToolBox

[Download Intellij](https://www.jetbrains.com/idea/)  
[Oracle JDK](https://www.oracle.com/java/technologies/)  
[Documentação Intellij](https://www.jetbrains.com/help/idea/discover-intellij-idea.html)

## Atalhos de teclado

***Double Shift*** - Encontrar rapidamente qualuqer arquivo, ação, simbolo, janela de ferramenta ou configuração no intellij, no projeto ou no repositorio local atual.  
***Ctrl+Shift+A*** - Encontrar um comando e executá-lo, abrir uma janela de ferramenta ou  pesquisar por uma configuração.  
***Alt+Enter*** - Mostrar as ações de contexto, correções rápidas para error e avisos.  
***F2*** - Navegar entre códigos.  
***Shift+F2*** - Pular para o proximo erro.  
***Ctrl+E*** - Ver os arquivos aberto recentemente.  
***Ctrl+Shift+Enter*** - Insira quaisquer símbolos finais necessários e coloque o circunflexo onde você pode começar a digitar a próxima instrução.  
***Ctrl+Alt+L*** - Formatar o código.  
***Ctrl+Alt+Shift+T*** - Refatorar o código.  
***Ctrl+W*** - Aumentar ou diminuir a seleção.  
***Ctrl+Shift+W*** - Aumente ou diminua o escopo da seleção de acordo com construções de código específicas.  
***Ctrl+/*** - Adicionar / remover linha ou bloquear comentário.  
***Ctrl+Shift+/*** - Comente uma linha ou bloco de código.  
***Ctrl+B*** - Vá para a declaração.  
***Alt+F7*** - Encontre usos.  
***Alt+1*** - Focar a janela da ferramenta de projeto.  

[voltar](../README.md)