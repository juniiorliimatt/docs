# Lista de docs

## Windows

- [Aplicativos windows](./docs/windows-apps.md)
- [WSL windows](./docs/wsl-ubuntu.md)

## Git

- [Comando úteis GIT](./docs/comandos-uteis-git.md)

## IDE's

- [Intellij](./docs/intellij.md)
- [Eclipse](./docs/eclipse.md)

## Configurações JPA

- [mysql](./docs/mysql-jpa-config.md)
- [postgres](./docs/postgresql-jpa-config.md)
- [oracle](./docs/oracle-jpa-config.md)

## Oracle Cloud

- [OCI](./docs/OCI.md)
- [OCI-Architecture](./docs/OCI-Architecture.md)

## Oracle Database

- [New User](./docs/new-user-oracledb.md)

## Spring Security

- [Spring Secutiry](./docs/spring-security.md)
